﻿var checkInterval = 1000*5;
var checkUrl = 'http://intranet.leoexpresswifi.com/intranet/fn/infoActualizer.php';
var infoUrl = 'http://intranet.leoexpresswifi.com/intranet/info/informace-o-jizde/';
var timer;

tick();

chrome.browserAction.setBadgeBackgroundColor({color : '#118e3a'});

chrome.browserAction.onClicked.addListener(function (tab) {
	openInfoTab();
	tick();
});

function tick() {
	ajax(checkUrl, handleResponse);
	if (timer) {clearTimeout(timer);}
	timer = setTimeout(function () {
		tick();
	}, checkInterval);
}

function handleResponse(response) {
	var data = new String(response).split("_");
	var speed = data[0];
	var temperature = data[1];
	var label = "Aktuální rychlost "+ speed + " km/h\nVenkovní teplota: "+ temperature +"°C"
	chrome.browserAction.setBadgeText({text : speed});
	chrome.browserAction.setTitle({title : label});
}

function handleError() {
	chrome.browserAction.setBadgeText({text : ''});
	chrome.browserAction.setTitle({title : ''});
}

function openInfoTab() {
	chrome.tabs.query({url : 'http://intranet.leoexpresswifi.com/*'}, function (foundTabs) {
		if (foundTabs[0]) {
			chrome.tabs.update(foundTabs[0].id, {selected: true})
		} else {
			chrome.tabs.create({url: infoUrl});
		}
	});
}

function ajax (url, callback) {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function () {
		if (xhr.readyState == 4) {
			var response = xhr.responseText;
			callback(response);
		}
	};
	xhr.open("GET", url, true);
	xhr.send();
}